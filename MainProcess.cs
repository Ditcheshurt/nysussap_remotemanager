﻿using System;
using System.Windows.Forms;

namespace NysusSAP_RemoteManager {
    static class MainProcess {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
