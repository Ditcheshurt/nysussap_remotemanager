﻿using System;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.IO.Pipes;
using System.Security.Cryptography;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;

namespace NysusSAP_RemoteManager {
    public partial class Form1 : Form {
        private static bool keepGoing = true;
        public StreamString ss;
        public NamedPipeClientStream[] npcs;

        public Form1() {
            InitializeComponent();
            Globals.rtb = richTextBox1;
            Globals.tb = textBox1;
            Globals.btn = btnSend;
            Globals.thd = new Thread(DoRemoteConsole);
            Globals.thd.Start();
        }
        public void DoRemoteConsole() {
            sbyte function = 0;
            npcs = new NamedPipeClientStream[] { new NamedPipeClientStream(".", "NysusSAPRCM_in", PipeDirection.Out), new NamedPipeClientStream(".", "NysusSAPRCM_out", PipeDirection.In) };
            Console.Write("Connecting to server...");
            try {
                npcs[0].Connect(5000);
                npcs[1].Connect(5000);
                Console.WriteLine("Connected!");
                Globals.InputEnabled(true);
                ss = new StreamString(npcs);
                ss.Initialize();
                ss.DoKeyExchange(24);

                while (keepGoing) {
                    string res = ss.ReadString(out function);
                    keepGoing = (function > 0);
                }
            } catch (Exception ex) {
                if (ex.GetType() == typeof(IOException)) {
                    Console.WriteLine("ERROR: {0}", ex.Message);
                    Globals.SetText(ex.ToString());
                } else if (ex.GetType() == typeof(TimeoutException)) {
                    Console.WriteLine("a timeout occurred while attempting to contact the server!");
                    Globals.SetText("A timeout occured while trying to connect to the SAP Connector service");
                } else { 
                    Globals.SetText(ex.ToString());
                }
            } finally {
                Disconnect();
                Globals.InputEnabled(false);
            }
        }
        private void Form1_Load(object sender, EventArgs e) {
            textBox1.Focus();
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            Disconnect();
        }
        private void Form1_tbKeyPress(object sender, KeyPressEventArgs e) {
            if (e.KeyChar == 13) ProcessInput(textBox1.Text);
        }
        private void btnSend_Click(object sender, EventArgs e) {
            ProcessInput(textBox1.Text);
        }
        private void ProcessInput(string str) {
            if (str.Length == 1) {
                ss.WriteString(str);
                //if (str == "x") Application.Exit();
            } else if (str.Contains("stop") || str.Contains("exit")) {
                ss.WriteString(str);
                richTextBox1.Text = "";
                richTextBox1.Enabled = false;
            }
            textBox1.Text = "";
        }
        private void Disconnect() {
            if(npcs != null && npcs[0].IsConnected && npcs[1].IsConnected) {
                npcs[1].Dispose();
                npcs[0].Dispose();
                npcs = null;
            }
        }
    }
    public static class Globals {
        public static RichTextBox rtb = null;
        public static TextBox tb = null;
        public static Button btn = null;
        public static Thread thd;
        delegate void SetRTBText(string text);
        delegate void TBEnabled(bool isEnabled);
        public static void SetText(string text) {
            if (rtb.InvokeRequired) {
                SetRTBText stxt = new SetRTBText(SetText);
                rtb.Invoke(stxt, new object[] { text });
            } else {
                rtb.Text = text;
            }
        }
        public static void InputEnabled(bool isEnabled) {
            if (tb.InvokeRequired) {
                TBEnabled disableInp = new TBEnabled(InputEnabled);
                tb.Invoke(disableInp, new object[] { isEnabled });
            } else {
                tb.Enabled = isEnabled;
                btn.Enabled = isEnabled;
            }
        }
    }
    public class StreamString {
        private Stream[] ioStream;
        private UnicodeEncoding streamEncoding;
        ConsoleMenu cm = null;

        private MemoryStream _ms;
        private RSACryptoServiceProvider RSA;
        private byte[] Exponent;
        private byte[] PublicKey;
        private byte[] SessionSymKey;
        private byte[] SessionSymIV;

        public StreamString(Stream[] ioStrmArr) {
            ioStream = ioStrmArr;
            streamEncoding = new UnicodeEncoding();
        }
        sealed class PreDeserializationBinder : SerializationBinder {
            public override Type BindToType(string assemblyName, string typeName) {
                Type typeToDeserialize = null;
                string thisAssm = Assembly.GetExecutingAssembly().FullName;
                string tn = thisAssm.Split(',')[0] + "." + typeName.Split('.')[1];
                typeToDeserialize = Type.GetType(String.Format("{0}, {1}", tn, thisAssm));
                return typeToDeserialize;
            }
        }
        public void Initialize() {
            RSA = new RSACryptoServiceProvider(2048);
            RSAParameters RSAKeyInfo = RSA.ExportParameters(false);
            Exponent = RSAKeyInfo.Exponent;
            PublicKey = RSAKeyInfo.Modulus;
        }
        public void DoKeyExchange(byte function) {
            int _len, _esk, _esiv;
            byte[] EncryptedSymKey, EncryptedSymIV, inBuffer;
            sbyte func;

            _len = PublicKey.Length + Exponent.Length;
            _ms = new MemoryStream(_len);
            _ms.Write(Exponent, 0, Exponent.Length);
            _ms.Write(PublicKey, 0, PublicKey.Length);
            _ms.Position = 0;

            StreamWrite((sbyte)function, _ms.ToArray(), false);
            inBuffer = StreamRead(out func, false);
            _len = inBuffer.Length;

            if (func == function) {
                _esk = inBuffer[0] * 256 + inBuffer[1];
                _esiv = (_len - 2) - _esk;

                EncryptedSymKey = new byte[_esk];
                EncryptedSymIV = new byte[_esiv];
                Buffer.BlockCopy(inBuffer, 2, EncryptedSymKey, 0, _esk);
                Buffer.BlockCopy(inBuffer, _esk + 2, EncryptedSymIV, 0, _esiv);

                SessionSymKey = RSA.Decrypt(EncryptedSymKey, false);
                SessionSymIV = RSA.Decrypt(EncryptedSymIV, false);
            }
        }
        private byte[] StreamRead(out sbyte function, bool useCrypto = false) {
            int _len;
            sbyte func = -1;
            byte[] inBuffer = null;

            func = (sbyte)ioStream[1].ReadByte();
            if (func > 0) { 
                _len = ioStream[1].ReadByte() * 256;
                _len += ioStream[1].ReadByte();
                inBuffer = new byte[_len];

                ioStream[1].Read(inBuffer, 0, _len);

                if (useCrypto) {
                    MemoryStream _cs = new MemoryStream(inBuffer);
                    RijndaelManaged RMCrypto = new RijndaelManaged();
                    RMCrypto.Padding = PaddingMode.PKCS7;
                    CryptoStream cs = new CryptoStream(_cs, RMCrypto.CreateDecryptor(SessionSymKey, SessionSymIV), CryptoStreamMode.Read);
                    MemoryStream _ms = new MemoryStream();
                    cs.CopyTo(_ms);
                    cs.Close();

                    inBuffer = new byte[_ms.Length]; _ms.Position = 0;
                    using (BinaryReader br = new BinaryReader(_ms)) {
                        br.Read(inBuffer, 0, (int)_ms.Length);
                    }
                }
            }
            function = func;
            return inBuffer;
        }
        private void StreamWrite(sbyte function, byte[] output, bool useCrypto = false) {
            MemoryStream _ms = new MemoryStream();
            var obs = new MemoryStream(output);
            byte[] outBuffer;

            if (useCrypto) {
                RijndaelManaged RMCrypto = new RijndaelManaged();
                RMCrypto.Padding = PaddingMode.PKCS7;
                CryptoStream cs = new CryptoStream(_ms, RMCrypto.CreateEncryptor(SessionSymKey, SessionSymIV), CryptoStreamMode.Write);

                obs.CopyTo(cs);
                cs.FlushFinalBlock();
                outBuffer = _ms.ToArray();
                cs.Close();
            } else {
                outBuffer = obs.ToArray();
            }

            int len = outBuffer.Length;
            if (len > ushort.MaxValue) {
                len = ushort.MaxValue;
            }

            ioStream[0].WriteByte((byte)function);
            ioStream[0].WriteByte((byte)(len / 256));
            ioStream[0].WriteByte((byte)(len & 255));
            ioStream[0].Write(outBuffer, 0, len);
            ioStream[0].Flush();

            _ms.Dispose();
        }
        public string ReadString(out sbyte function) {
            string result = "";
            sbyte func = -1;

            byte[] inBuffer = StreamRead(out func, true);

            if (func == 5 || func == 10) {
                result = streamEncoding.GetString(inBuffer);
            } else if (func == 42) {
                var bf = new BinaryFormatter();
                bf.Binder = new PreDeserializationBinder();
                cm = (ConsoleMenu)bf.Deserialize(new MemoryStream(inBuffer));
                cm.isConsole = true;
                cm.Show();
            } else if (func == -1) {
                Application.Exit();
            }
            function = func;
            return result;
        }
        public int WriteString(string outString) {
            byte[] outBuffer = streamEncoding.GetBytes(outString);
            StreamWrite(44, streamEncoding.GetBytes(outString), true);
            return outBuffer.Length + 2;
        }
    }
}
