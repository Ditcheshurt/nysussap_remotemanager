﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;

namespace NysusSAP_RemoteManager
{
    [Serializable]
    class ConsoleMenu {
        public string MenuSelectorText;
        public string MenuAlert;
        public string MenuMessageAreaTitle;
        public int MenuMaxMessages;
        public bool MenuHasAlertArea;
        public bool MenuHasMessageArea;
        public bool MenuIsActive;
        public bool isConsole = true;

        private List<string> MenuItems;
        private List<string> MenuMessages;
        private string MenuTitle;
        private int menuCurLn, curSelX, curSelY;
        private bool showMenuAlert;

        public ConsoleMenu(string title, bool console){
            MenuTitle = "====== " + title + " ======";
            MenuItems = new List<string>();
            MenuMessages = new List<string>();
        }
        public void AddMenuItem(string selector, string msg = "") {
            if (msg.Length > 0){
                MenuItems.Add(" " + selector + " " + msg);
            }else{
                MenuItems.Add("--------------------");
            }
        }
        public void ModifyMenuItem(int intItem, string msg) {
            if(msg.Length > 0 && intItem < MenuItems.Count) {
                string t = MenuItems[intItem];
                string s = t.Substring(1, 1);
                MenuItems[intItem] = " " + s + " " + msg;
            } 
        }
        public void AddMenuMessage(string msg) {
            if (msg.Length > 0) MenuMessages.Add(msg);
            if (MenuMessages.Count > MenuMaxMessages) MenuMessages.RemoveRange(0, MenuMessages.Count - MenuMaxMessages);
            Show();
        }
        public void Show() {
            string msg = "";
            if (isConsole) { 
                if(MenuIsActive) {
                    menuCurLn = 0;
                    Globals.SetText("");
                    msg += MenuTitle + "\n";
                    foreach(string mi in MenuItems) {
                        msg += mi + "\n";
                    }
                    msg += "\n" + MenuSelectorText;
                    if(MenuHasAlertArea){
                        if (MenuAlertShown){
                            msg += "\n" + MenuAlert;
                        }
                    }
                    if(MenuHasMessageArea){
                        msg += "\n" + MenuMessageAreaTitle;
                        foreach (string str in MenuMessages) {
                            msg += "\n" + str;
                        }
                    }
                    Globals.SetText(msg);
                }
            }
        }
        public bool GetInput(out string str) { // will (eventually) return true if valid selection
            string input = "";
            if (isConsole) {
                input = Console.ReadLine();
            }
            if (input == null) str = "";
            bool valid = false;
            foreach(string mi in MenuItems) {
                if(mi.Substring(0,3).Contains(input) && !input.Equals("-")){
                    valid = true;
                    break; // valid selector so break out of the foreach loop
                }
            }
            str = input;
            return valid;
        }
        public bool MenuAlertShown{
            get { return showMenuAlert; }
            set { showMenuAlert = value; Show(); }
        }
    }
}
